# List installed items
alias brews="brew list"

# Update, upgrade & clean up in one go
alias bubu="brew update && brew upgrade && brew cleanup"
